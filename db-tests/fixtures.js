
import path from 'path';
import * as fs from 'fs';
import https from 'https';
import { fileURLToPath } from 'url';

export const mochaGlobalSetup = async () => {
    console.log('Start global setup');
    const downloadUrl = 'https://drive.google.com/u/0/uc?id=1sEhxCqCVcjXornBzDEW1vr4xbG7vLh3T&export=download';
    const response = await makeRequest(downloadUrl);
    const __dirname = path.dirname(fileURLToPath(import.meta.url));
    const filePath = path.join(__dirname, 'college.db');
    const file = fs.createWriteStream(filePath);
    const fileResponse = await makeRequest(response.headers.location);
    fileResponse.pipe(file);
    await new Promise(resolve => file.on('finish', () => {
        console.log(`File ready: ${filePath}`);
        resolve();
    }));
};

export const mochaGlobalTeardown = () => {
    console.log('Global teardown fixture!');
};

async function makeRequest(url) {
    return new Promise((resolve, reject) => {
        https.get(url, function (response) {
            resolve(response);
        }).on('error', (e) => {
            reject(e);
        });
    });
}