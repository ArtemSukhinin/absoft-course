module.exports = {
    ui: 'tdd',
    require: [
        './db-tests/hooks.js',
        './db-tests/fixtures.js'
    ],
    spec: ['./db-tests/*.spec.js']
};