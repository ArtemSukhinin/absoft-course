import sqlite3 from 'sqlite3';
import path from 'path';
import { fileURLToPath } from 'url';

function suiteSetup(done) {
  console.log('suite setup by hook');
  const __dirname = path.dirname(fileURLToPath(import.meta.url));
  const dbPath = path.join(__dirname, 'college.db');
  this.db = new sqlite3.Database(dbPath, sqlite3.OPEN_READONLY, (err) => {
    if (err) {
      console.error(err.message);
    }
    console.log(`Connected to the database: ${dbPath}`);
    done();
  });
}

function setup() {
  console.log('test setup by hook');
  
}

function teardown() {
  console.log('test teardown by hook');
}

function suiteTeardown(done) {
  console.log('suite teardown by hook');
  this.db.close((err) => {
    if (err) {
      return console.error(err.message);
    }
    console.log('Close the database connection.');
    done();
  });
}

export const mochaHooks = {
  beforeAll: [suiteSetup],
  beforeEach: [setup],
  afterAll: [suiteTeardown],
  afterEach: [teardown],
};
