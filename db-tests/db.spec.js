import { assert } from 'chai';
suite('DB Suite', function () {
    test('DB-1', async function () {
        const db = this.db;
        const sql = 'PRAGMA table_info("STUDENTS");';
        const rows = await execute(db, sql);
        const actualTypes = rows.reduce((acc, cur) => {
            acc[cur.name] = cur.type;
            return acc;
        }, {});
        const expectedTypes = {
            ID: 'INTEGER',
            FIRST_NAME: 'TEXT',
            LAST_NAME: 'TEXT',
            DEPARTMENT: 'TEXT',
            SCORE: 'INTEGER',
        };
        assert.deepEqual(actualTypes, expectedTypes);
    });
});

function execute(db, sql) {
    return new Promise((resolve, reject) => {
        db.all(sql, [], (err, rows) => {
            if (err) {
                reject(err);
            }
            resolve(rows);
        });
    });
}
